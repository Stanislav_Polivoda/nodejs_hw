const express=require("express");
const morgan=require("morgan");

const fs = require("fs");



const app=express();
app.use(morgan("tiny"));
app.use(express.text());
app.use(express.json());


const saved_files_names=[];

app.post("/api/files",(req,res,next)=>{
const {filename, content}=req.body;

saved_files_names.push(filename);
fs.writeFileSync(`files/${filename}`, `${content}`,"utf-8");
res.status(200).json({message:"message"});

    });

app.get("/api/files",(req,res)=>{
    //const content= fs.readFileSync("files/test.txt","utf-8");
    res.json({content:`${saved_files_names}`});
})



app.get("/api/files/:filename",(req,res)=>{
    try{
    let need_file=(req.params["filename"]);
    const content= fs.readFileSync(`files/${need_file}`,"utf-8");
    res.json({content:`${content}`});
    }
    catch(error){
        return res.status(400).json({"message": 'request error'});
    }
})



app.listen(8080,()=>{
    console.log("server works")
})
